from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class Measurements(models.Model):
    MEAS_STATUS_CHOICES = (
        ('submitted','Submitted'),
        ('processed','Processed'),
        ('retracted','Retracted'),
    )
    FILE_STATUS_CHOICES = (
        ('undetermined','Undetermined'),
        ('valid','Valid'),
        ('invalid','Invalid'),
    )
    DEVICE_CHOICES = (
        ('undetermined','Undetermined'),
        ('licor','Licor'),
        ('gasmet','Gasmet'),
        ('egm4','EGM4'),
        ('egm5','EGM5'),
    )
    measurer      = models.ForeignKey(User, on_delete=models.PROTECT,
                                      related_name='submissions')
    number        = models.IntegerField()
    date          = models.DateTimeField(default=timezone.now)
    measure_date  = models.DateField()
    status        = models.CharField(max_length=10, choices=MEAS_STATUS_CHOICES,
                                     default='submitted')
    comment       = models.CharField(max_length=256,default='')
    device        = models.CharField(max_length=12, choices=DEVICE_CHOICES,
                                     default='undetermined')
    datastatus    = models.CharField(max_length=12, choices=FILE_STATUS_CHOICES,
                                     default='undetermined')
    datafilepath  = models.CharField(max_length=50,default='NA')
    dataorigname  = models.CharField(max_length=50,default='')
    fieldstatus   = models.CharField(max_length=12, choices=FILE_STATUS_CHOICES,
                                     default='undetermined')
    fieldfilepath = models.CharField(max_length=50,default='NA')
    fieldorigname = models.CharField(max_length=50,default='')
