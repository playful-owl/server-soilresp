from django import forms

YEARS = [x for x in range(2020,2022)]

class MeasurementsForm(forms.Form):
    measure_date = forms.DateField(label='Measurement date', initial="2021-01-01",
                                   widget=forms.SelectDateWidget(years=YEARS))
    comment = forms.CharField(max_length=255,required=False)
    datafile = forms.FileField(label='Data file')
    fieldfile = forms.FileField(label='Field form')

class ChangeRecordForm(forms.Form):
    new_measure_date = forms.DateField(label='Measurement date', initial="2021-01-01",
                                       widget=forms.SelectDateWidget(years=YEARS))
    new_comment = forms.CharField(max_length=255,required=False)
