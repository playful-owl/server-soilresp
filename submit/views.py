import numpy
import pandas
import datetime

from django.http import HttpResponse, HttpResponseRedirect, FileResponse, JsonResponse
from django.contrib import messages
from django.template import loader
from django.shortcuts import render, get_object_or_404

from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required, user_passes_test

from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from .models import Measurements
from .forms import MeasurementsForm, ChangeRecordForm
from .uploads import handle_uploaded_file
from .filecheck import *

def index(request):
    template = loader.get_template('submit/index.html')
    context = { 'a': 3 }
    return HttpResponse(template.render(context, request))

@user_passes_test(lambda user: user.is_staff)
def measurements_list(request):
    measurements = Measurements.objects.all().order_by('measurer','-measure_date')
    return render(request,'submit/meas/list.html',
                  {'measurements': measurements})

@login_required
def upload(request):
    if request.method == 'POST':
        form = MeasurementsForm(request.POST,request.FILES)
        if form.is_valid():
            if request.FILES['datafile'].size > 6000000:
                messages.info(request,'Upload failed, datafile size must be under 6 MB')
            elif request.FILES['fieldfile'].size > 6000000:
                messages.info(request,'Upload failed, fieldform size must be under 6 MB')
            else:
                current_user       = request.user
                userid: int        = current_user.id
                comment: str       = request.POST.get("comment", "")
                md_year: str       = request.POST.get("measure_date_year")
                md_month: str      = request.POST.get("measure_date_month")
                md_day: str        = request.POST.get("measure_date_day")
                measdate           = datetime.date(int(md_year), int(md_month), int(md_day))
                datafilename: str  = request.FILES['datafile'].name
                fieldfilename: str = request.FILES['fieldfile'].name
                accepted_datafile_exts = ['.txt','.TXT','.dat','.DAT','data','DATA',
                                          '.csv','.CSV','text','TEXT']
                if fieldfilename[-5:] != '.xlsx':
                    messages.info(request,
                                  'Field form file extension must be .xlsx !')
                elif datafilename[-4:] not in accepted_datafile_exts:
                    messages.info(request,
                                  'Datafile extension must be one of {0}'
                                  .format(" ".join(accepted_datafile_exts)))
                elif len(fieldfilename) > 50:
                    messages.info(request,
                                  'field form file name exceeds maximum of 50 characters')
                elif len(datafilename) > 50:
                    messages.info(request,
                                  'data file name exceeds maximum of 50 characters')
                else:
                    res = handle_uploaded_file(request.FILES['datafile'],
                                               request.FILES['fieldfile'],
                                               datafilename,fieldfilename,
                                               measdate,userid,comment)
                    if res['ok']:
                        messages.info(request,
                                      'Uploaded files ' + datafilename +
                                      ', ' + fieldfilename)
                    elif res.get('msg'):
                        messages.info(request,'errors: ' + str(res.get('msg')))
                    else:
                        messages.info(request,'upload fails with an undetermined error')
            return HttpResponseRedirect('/submit/upload/')
        else:
            messages.info(request,'error: form not valid')
    else:
        current_user = request.user
        userid = current_user.id
        uobj = User.objects.get(pk=userid)
        measurements = Measurements.objects.filter(measurer=userid).order_by('-measure_date')
        form = MeasurementsForm()
    return render(request, 'submit/meas/upload.html',
                  {'form': form,
                   'measurements': measurements })

## detail views and file sending

def formdetail(request, uname, num):
    if request.method == 'POST':
        form = ChangeRecordForm(request.POST)
        if form.is_valid():
            comm: str     = request.POST.get("new_comment", "")
            md_year: str  = request.POST.get("new_measure_date_year")
            md_month: str = request.POST.get("new_measure_date_month")
            md_day: str   = request.POST.get("new_measure_date_day")
            measdate      = datetime.date(int(md_year), int(md_month), int(md_day))
            uobj = User.objects.filter(username=uname).first()
            Measurements.objects.filter(measurer=uobj.id).filter(number=num).update(measure_date = measdate)
            Measurements.objects.filter(measurer=uobj.id).filter(number=num).update(comment = comm)
            print("toimii")
            messages.info(request,'Changed details of submission')
        current_url: str = '/submit/fdetails/' + uname + '/' + str(num) + '/'
        return HttpResponseRedirect(current_url)
    else:
        uobj = User.objects.filter(username=uname).first()
        submission = Measurements.objects.filter(measurer=uobj.id).filter(number=num).first()
        mstatus = submission.status
        ffpath = submission.fieldfilepath
        ffm = check_ff_magic(ffpath)
        ffp = None
        dimensions = None
        details = []
        validity = False
        if ffm:
            ffp = get_ff_pandas(ffpath,submission.device)
            if ffp['ok']:
                df = ffp.get('df')
                dimensions = ffp.get('dims')
                df = df[["Date (yyyy-mm-dd)","Monitoring site ID","Sub-site ID",
                         "Monitoring point ID","Start time","Start ppm","End time",
                         "End ppm","Chamber start T, C","Chamber end T, C",
                         "Chamber volume, dm3","Chamber ID"]]
                df = df.rename(columns={"Date (yyyy-mm-dd)":"date",
                                        "Monitoring site ID":"siteid",
                                        "Sub-site ID":"subsiteid",
                                        "Monitoring point ID":"point",
                                        "Start time":"start_time",
                                        "End time":"end_time",
                                        "Start ppm":"start_ppm",
                                        "End ppm":"end_ppm",
                                        "Chamber start T, C":"start_temp",
                                        "Chamber end T, C":"end_temp",
                                        "Chamber volume, dm3":"chamber_vol",
                                        "Chamber ID":"chamber_id"})
                ch_ids = df["chamber_id"].to_list()
                try:
                    rads = [chamber_dimensions.get(ch_ids[i])[0] for i in range(len(ch_ids))]
                    areas = pandas.Series([pi * r * r for r in rads],index=df.index)
                except:
                    areas = pandas.Series([numpy.nan for i in range(dimensions[0])],
                                          index=df.index)
                    validity = False
                else:
                    validity = True
                df["chamber_area"] = areas
                details = df.to_dict('records')
        if validity:
            if submission.fieldstatus != 'valid':
                Measurements.objects.filter(measurer=uobj.id).filter(number=num).update(fieldstatus = 'valid')
        else:
            if submission.fieldstatus != 'invalid':
                Measurements.objects.filter(measurer=uobj.id).filter(number=num).update(fieldstatus = 'invalid')
        form = ChangeRecordForm()
        return render(request, 'submit/meas/fdetail.html',
                      {'form': form,
                       'submission': submission,
                       'ff_m_ok': ffm,
                       'ff_p_ok': ffp.get('ok'),
                       'ff_p_startend_ok':   ffp.get('startend_ok'),
                       'ff_p_datetime_ok':   ffp.get('datetime_ok'),
                       'ff_p_durations_ok':  ffp.get('durations_ok'),
                       'ff_p_siteids_ok':    ffp.get('siteid_ok'),
                       'ff_p_subsiteids_ok': ffp.get('subsiteid_ok'),
                       'ff_p_chamberids_ok': ffp.get('chamberid_ok'),
                       'ff_p_numerics_ok':   ffp.get('numerics_ok'),
                       'ff_p_volume_ok':     ffp.get('volume_ok'),
                       'validity': validity,
                       'dims': dimensions,
                       'status': mstatus,
                       'details': details})

def datadetail(request, uname, num):
    uobj = User.objects.filter(username=uname).first()
    submission = Measurements.objects.filter(measurer=uobj.id).filter(number=num).first()
    dfpath = submission.datafilepath
    ffpath = submission.fieldfilepath
    device = submission.device
    check_magic = check_df_magic(dfpath)
    check_read = None
    dimensions = None
    gases_present = None
    details = []
    validity = False
    start_times = []
    nrows = []
    if check_magic:
        dfp = {'ok': False}
        if device == 'undetermined':
            found_new = False
            if check_df_is_gasmet(dfpath):
                Measurements.objects.filter(measurer=uobj.id).filter(number=num).update(device = 'gasmet')
                device = 'gasmet'
                found_new = True
            if check_df_is_licor(dfpath):
                Measurements.objects.filter(measurer=uobj.id).filter(number=num).update(device = 'licor')
                device = 'licor'
                found_new = True
            if check_df_is_egm5(dfpath):
                Measurements.objects.filter(measurer=uobj.id).filter(number=num).update(device = 'egm5')
                device = 'egm5'
                found_new = True
            if chech_df_is_egm4(dfpath):
                Measurements.objects.filter(measurer=uobj.id).filter(number=num).update(device = 'egm4')
                device = 'egm4'
                found_new = True
            if found_new:
                dfp['msg'] = 'device set to ' + device + ', please reload this page'
        elif device == 'egm5':
            dfp = read_df_egm5(dfpath,True)
        elif device == 'egm4':
            dfp = read_df_egm4(dfpath,True)
        elif device == 'licor':
            dfp = read_df_licor(dfpath,True)
            gases_present = dfp.get('present')
        elif device == 'gasmet':
            dfp = read_df_gasmet(dfpath,True)
        else:
            dfp['msg'] = 'submission.device: ' + device + ', not egm5, licor, gasmet'
        if dfp['ok']:
            check_read = True
            dimensions = dfp.get('dims')
            validity = True
            ddf = dfp.get('df')
            fdf_out = get_ff_pandas(ffpath,device)
            fdf = fdf_out['df']
            len_series = len(fdf["Date (yyyy-mm-dd)"].to_list())
            for i in range(len_series):
                start_time = fdf.iloc[i, fdf.columns.get_loc('Start time')]
                end_time   = fdf.iloc[i, fdf.columns.get_loc('End time')]
                meas_date  = fdf.iloc[i, fdf.columns.get_loc('Date (yyyy-mm-dd)')]
                if device == 'licor':
                    part = ddf[(ddf['DATE'] == meas_date) &
                               (ddf['TIME'] > start_time) &
                               (ddf['TIME'] < end_time)]
                else:
                    part = ddf[(ddf['Date'] == meas_date) &
                               (ddf['Time'] >= start_time) &
                               (ddf['Time'] <= end_time)]
                part_shape = part.shape
                start_times.append(str(start_time))
                nrows.append(str(part_shape[0]))
        else:
            messages.info(request,'Read fails with message: ' + str(dfp.get('msg')))
    if validity:
        if submission.datastatus != 'valid':
            Measurements.objects.filter(measurer=uobj.id).filter(number=num).update(datastatus = 'valid')
    else:
        if submission.datastatus != 'invalid':
            Measurements.objects.filter(measurer=uobj.id).filter(number=num).update(datastatus = 'invalid')
    return render(request, 'submit/meas/ddetail.html',
                  {'submission': submission,
                   'df_m_ok': check_magic,
                   'df_h_ok': check_read,
                   'gases': gases_present,
                   'validity': validity,
                   'dims': dimensions,
                   'start_times': start_times,
                   'nrows': nrows})

def sendformfile(response, uname, num):
    uobj = User.objects.filter(username=uname).first()
    submission = Measurements.objects.filter(measurer=uobj.id).filter(number=num).first()
    origname = submission.fieldorigname
    cdisp = 'inline; filename="' + origname + '"'
    resp = FileResponse(open(submission.fieldfilepath,'rb'))
    resp['Content-Disposition'] = cdisp
    return resp

def senddatafile(response, uname, num):
    uobj = User.objects.filter(username=uname).first()
    submission = Measurements.objects.filter(measurer=uobj.id).filter(number=num).first()
    origname = submission.fieldorigname
    cdisp = 'inline; filename="' + origname + '"'
    resp = FileResponse(open(submission.datafilepath,'rb'))
    resp['Content-Disposition'] = cdisp
    return resp

## detail ajax views

## todo: this route should be POST and require login
def statuschange(request):
    uname = request.GET.get('uname')
    uobj = User.objects.filter(username=uname).first()
    num = request.GET.get('num')
    submission = Measurements.objects.filter(measurer=uobj.id).filter(number=num).first()
    old_status = submission.status
    out = {'ok': 'true'}
    if old_status == 'submitted':
        Measurements.objects.filter(measurer=uobj.id).filter(number=num).update(status = 'retracted')
    elif old_status == 'retracted':
        Measurements.objects.filter(measurer=uobj.id).filter(number=num).update(status = 'submitted')
    else:
        out['ok'] = 'false'
    return JsonResponse(out)

def dataget(request):
    uname = request.GET.get('uname')
    uobj = User.objects.filter(username=uname).first()
    num = request.GET.get('num')
    submission = Measurements.objects.filter(measurer=uobj.id).filter(number=num).first()
    dfpath = submission.datafilepath
    out = {'ok': 'false'}
    if submission.device == 'egm5': ## todo: these could be more concise
        out['device'] = 'egm5'
        dfp = read_df_egm5(dfpath,True)
        if dfp['ok']:
            df = dfp['df']
            dimensions = dfp.get('dims')
            out['ok'] = 'true'
            out['skiprows'] = dfp.get('skiprows')
            time = []
            co2 = []
            for i in range(dimensions[0]):
                if i in dfp['skiprows']:
                    continue
                else:
                    time.append(df.iloc[i,df.columns.get_loc('Time')])
                    co2.append(int(df.iloc[i,df.columns.get_loc('CO2')]))
                    out['time'] = time
                    out['co2'] = co2
    elif submission.device == 'egm4':
        out['device'] = 'egm4'
        dfp = read_df_egm4(dfpath,True)
        if dfp['ok']:
            df = dfp['df']
            dimensions = dfp.get('dims')
            out['ok'] = 'true'
            out['skiprows'] = dfp.get('skiprows')
            index = []
            co2 = []
            for i in range(dimensions[0]):
                if i in dfp['skiprows']:
                    continue
                else:
                    index.append(i)
                    co2.append(int(df.iloc[i,df.columns.get_loc('CO2')]))
                    out['time'] = index
                    out['co2'] = co2
    elif submission.device == 'licor':
        out['device'] = 'licor'
        dfp = read_df_licor(dfpath,True)
        if dfp['ok']:
            df = dfp['df']
            out['ok'] = 'true'
            out['time'] = df["TIME"].to_list()
            out['co2'] = df["CO2"].to_list()
    elif submission.device == 'gasmet':
        out['device'] = 'gasmet'
        dfp = read_df_gasmet(dfpath,True)
        if dfp['ok']:
            df = dfp['df']
            out['ok'] = 'true'
            out['time'] = df['Time'].to_list()
            out['co2'] = df['Carbon dioxide CO2'].to_list()
    return JsonResponse(out)

## API access

@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
@authentication_classes((JSONWebTokenAuthentication,))
def get_users(request):
    uobjs = User.objects.all()
    data = {
        'ids': [u.id for u in uobjs],
        'usernames': [u.username for u in uobjs],
    }
    return JsonResponse(data)

@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
@authentication_classes((JSONWebTokenAuthentication,))
def get_submissions(request):
    submissions = Measurements.objects.all()
    data = {'num': len(submissions)} ## todo: remove
    data['data'] = [{'id': s.id,
                     'number': s.number,
                     'date': s.date,
                     'measure_date': s.measure_date,
                     'device': s.device,
                     'fieldfilename': s.fieldorigname,
                     'datafilename': s.dataorigname,
                     'status': s.status,
                     'datastatus': s.datastatus,
                     'fieldstatus': s.fieldstatus,
                     'comment': s.comment}
                    for s in submissions]
    return JsonResponse(data)

@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
@authentication_classes((JSONWebTokenAuthentication,))
def get_submissions_plus(request):
    submissions = Measurements.objects.all()
    sites = ['NA']*len(submissions)
    for i in range(len(submissions)):
        s = submissions[i]
        s_status = s.status
        ffpath = s.fieldfilepath
        ffp = get_ff_pandas(ffpath,s.device)
        if ffp['ok']:
            df = ffp.get('df')
            siteids = df["Monitoring site ID"].to_list()
            sites[i] = siteids[0]
    data = {'num': len(submissions)} ## todo: remove
    data['data'] = [{'measurer': s.measurer.username,
                     'status': s.status,
                     'id': s.id,
                     'siteid': i,
                     'number': s.number,
                     'date': s.date,
                     'measure_date': s.measure_date,
                     'device': s.device,
                     'fieldfilename': s.fieldorigname,
                     'datafilename': s.dataorigname,
                     'comment': s.comment}
                    for s,i in zip(submissions,sites)]
    return JsonResponse(data)

@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
@authentication_classes((JSONWebTokenAuthentication,))
def get_submissions_uid(request, uid):
    submissions = Measurements.objects.filter(measurer=uid).all()
    data = {
        'num': len(submissions),
        'numbers': [s.number for s in submissions],
        'dates': [s.date for s in submissions],
        'measure_dates': [s.measure_date for s in submissions],
        'fieldfilenames': [s.fieldorigname for s in submissions],
        'datafilenames': [s.dataorigname for s in submissions]
    }
    return JsonResponse(data)

@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
@authentication_classes((JSONWebTokenAuthentication,))
def get_submissions_uname(request, uname):
    uobj = User.objects.filter(username=uname).first()
    submissions = Measurements.objects.filter(measurer=uobj.id).all()
    data = {
        'num': len(submissions),
        'numbers': [s.number for s in submissions],
        'dates': [s.date for s in submissions],
        'measure_dates': [s.measure_date for s in submissions],
        'fieldfilenames': [s.fieldorigname for s in submissions],
        'datafilenames': [s.dataorigname for s in submissions]
    }
    return JsonResponse(data)

## this is used by the data server
@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
@authentication_classes((JSONWebTokenAuthentication,))
def get_submission(request, mid):
    data = {}
    s = Measurements.objects.filter(id=mid).first()
    if s is not None:
        data['ok'] = True
        data['data'] = {
            'id':            s.id,
            'date':          s.date,
            'measure_date':  s.measure_date,
            'status':        s.status,
            'device':        s.device,
            'fieldfilename': s.fieldorigname,
            'datafilename':  s.dataorigname,
            'datastatus':    s.datastatus,
            'fieldstatus':   s.fieldstatus,
            'comment':       s.comment
        }
    else:
        data['ok'] = False
        data['msg'] = "No submission with id " + str(mid)
    return JsonResponse(data)

## this is used by the data server
@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
@authentication_classes((JSONWebTokenAuthentication,))
def get_field_data_id(request, mid):
    data = {}
    s = Measurements.objects.filter(id=mid).first()
    if not s:
        data['msg'] = 'no series with id ' + str(mid)
        data['ok'] = False
        return JsonResponse(data)
    ffp = get_ff_pandas(s.fieldfilepath,s.device)
    if ffp['ok']:
        df0 = ffp.get('df')
        ch_ids = df0["Chamber ID"].to_list()
        try:
            rads = [chamber_dimensions.get(ch_ids[i])[0] for i in range(len(ch_ids))]
            areas = pandas.Series([pi * r * r for r in rads],index=df0.index)
            df0["chamber_area"] = areas
        except:
            data['msg'] = 'could not get numeric area values'
            data['ok'] = 'false'
            return JsonResponse(data)
        df = df0.filter(["Date (yyyy-mm-dd)","Monitoring site ID","Sub-site ID",
                         "Monitoring point ID","Monitoring point type","Site description",
                         "Start time","Start ppm","End time","End ppm",
                         "Chamber start T, C","Chamber end T, C",
                         "T at 05, C","T at 10, C","T at 15, C",
                         "T at 20, C","T at 30, C","T at 40, C",
                         "Soil moisture at topsoil","Chamber setting",
                         "Chamber volume, dm3","WT real, cm",
                         "Notes_1","Notes_2","Notes_3",
                         "Use of fabric on the monitoring point",
                         "Weather","Wind","chamber_area"])
        df.rename(columns={"Date (yyyy-mm-dd)":"date",
                           "Monitoring site ID":"siteid",
                           "Sub-site ID":"subsiteid",
                           "Monitoring point ID":"point",
                           "Monitoring point type":"pointtype",
                           "Site description":"sitedesc",
                           "Start time":"start_time",
                           "End time":"end_time",
                           "Start ppm":"start_ppm",
                           "End ppm":"end_ppm",
                           "Chamber start T, C":"start_temp",
                           "Chamber end T, C":"end_temp",
                           "T at 05, C":"t05",
                           "T at 10, C":"t10",
                           "T at 15, C":"t15",
                           "T at 20, C":"t20",
                           "T at 30, C":"t30",
                           "T at 40, C":"t40",
                           "Soil moisture at topsoil":"tsmoisture",
                           "Chamber setting":"chambersetting",
                           "Chamber volume, dm3":"chamber_vol",
                           "WT real, cm":"wt",
                           "Notes_1":"notes1",
                           "Notes_2":"notes2",
                           "Notes_3":"notes3",
                           "Use of fabric on the monitoring point":"fabric",
                           "Weather":"weather",
                           "Wind":"wind"},inplace=True)
        df["point"] = df["point"].astype(str)
        res = df.to_dict('records')
        data['data'] = res
        data['ok'] = 'true'
    else:
        data['msg'] = 'pandas field form reading failed'
        data['ok'] = 'false'
    return JsonResponse(data)

@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
@authentication_classes((JSONWebTokenAuthentication,))
def get_field_data_uid(request, uid, num):
    s = Measurements.objects.filter(measurer=uid).filter(number=num).first()
    ffp = get_ff_pandas(s.fieldfilepath,s.device)
    if ffp['ok']:
        df = ffp['df']
        datasize = len(df["Date (yyyy-mm-dd)"].to_list())
        dimensions = ffp.get('dims')
        data = {
            'ok':         True,
            'size':       datasize,
            'date':       df["Date (yyyy-mm-dd)"].to_list(),
            'siteid':     df["Monitoring site ID"].to_list(),
            'subsiteid':  df["Sub-site ID"].to_list(),
            'start_time': df["Start time"].to_list(),
            'end_time':   df["End time"].to_list(),
            'start_ppm':  df["Start ppm"].to_list(),
            'end_ppm':    df["End ppm"].to_list(),
        }
    else:
        data = {'ok': False}
    return JsonResponse(data)

## this is used by the data server
@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
@authentication_classes((JSONWebTokenAuthentication,))
def get_data_series(request, mid, series_num):
    out = {'ok': 'false', 'data': {}}
    s = Measurements.objects.filter(id=mid).first()
    ffp = get_ff_pandas(s.fieldfilepath,s.device)
    if ffp['ok']:
        fdf = ffp['df']
        datasize = len(fdf["Date (yyyy-mm-dd)"].to_list()) ## why not fdf.shape[0] ?
        if series_num >= datasize:
            out['msg'] = 'series index out of upper bound'
            return JsonResponse(out)
        elif series_num < 0:
            out['msg'] = 'series index out of lower bound'
            return JsonResponse(out)
        start_time = fdf.iloc[series_num, fdf.columns.get_loc('Start time')]
        end_time = fdf.iloc[series_num, fdf.columns.get_loc('End time')]
        meas_date = fdf.iloc[series_num, fdf.columns.get_loc('Date (yyyy-mm-dd)')]
        out['start_time'] = start_time
        out['end_time'] = end_time
        out['date'] = meas_date
        if s.device == 'egm5':
            ## todo: use meas_date
            dfp = read_df_egm5(s.datafilepath,True)
            out['device'] = 'egm5'
            if dfp['ok']:
                ddf = dfp['df']
                part = ddf[(ddf['Date'] == meas_date) &
                           (ddf['Time'] > start_time) &
                           (ddf['Time'] < end_time)]
                out['data']['time'] = part["Time"].to_list()
                out['data']['co2'] = part["CO2"].to_list()
                out['ok'] = 'true'
            else:
                out['msg'] = dfp.get('msg')
        elif s.device == 'egm4':
            dfp = read_df_egm4(s.datafilepath,True)
            out['device'] = 'egm4'
            if dfp['ok']:
                ddf = dfp['df']
                nr = dfp['num_records']
                counts = [ddf[(ddf['rec_index'] == i) &
                              (ddf['Date'] == meas_date) &
                              (ddf['Time'] >= start_time) &
                              (ddf['Time'] <= end_time)].shape[0]
                          for i in range(nr)]
                idx = counts.index(max(counts))
                part = ddf[(ddf['rec_index'] == idx)]
                out['data']['recno'] = part['RecNo'].to_list()
                out['data']['time'] = part['Time'].to_list()
                out['data']['co2'] = part['CO2'].to_list()
                out['ok'] = True
            else:
                out['msg'] = dfp.get('msg')
        elif s.device == 'licor':
            dfp = read_df_licor(s.datafilepath,True)
            out['device'] = 'licor'
            if dfp['ok']:
                ddf = dfp['df']
                gases = dfp.get('present')
                part = ddf[(ddf['DATE'] == meas_date) &
                           (ddf['TIME'] > start_time) &
                           (ddf['TIME'] < end_time)]
                out['data']['time'] = part["TIME"].to_list()
                if 'CO2' in gases:
                    out['data']['co2'] = part["CO2"].to_list()
                if 'CH4' in gases:
                    out['data']['ch4'] = part["CH4"].to_list()
                if 'N2O' in gases:
                    out['data']['n2o'] = part["N2O"].to_list()
                out['ok'] = 'true'
            else:
                out['msg'] = dfp.get('msg')
        elif s.device == 'gasmet':
            dfp = read_df_gasmet(s.datafilepath,True)
            out['device'] = 'gasmet'
            if dfp.get('ok'):
                ddf = dfp['df']
                part = ddf[(ddf['Date'] == meas_date) &
                           (ddf['Time'] > start_time) &
                           (ddf['Time'] < end_time)]
                out['data']['time'] = part['Time'].to_list()
                out['data']['co2'] = part['Carbon dioxide CO2'].to_list()
                if 'Methane CH4' in part.columns:
                    ch4_list = part['Methane CH4'].to_list()
                    out['data']['ch4'] = [ch4_list[i]*1000.0 for i in range(len(ch4_list))]
                if 'Nitrous oxide N2O' in part.columns:
                    n2o_list = part['Nitrous oxide N2O'].to_list()
                    out['data']['n2o'] = [n2o_list[i]*1000.0 for i in range(len(n2o_list))]
                out['ok'] = 'true'
            else:
                out['msg'] = dfp.get('msg')
        else:
            print('get_data_series: device not supported')
    return JsonResponse(out)
