import datetime
from pathlib import Path
from django.contrib.auth.models import User
from .models import Measurements
from .filecheck import *

def handle_uploaded_file(datafile,fieldfile,dataname,fieldname,
                         measdate,uid,comment_str):
    ## get next free number
    uobj = User.objects.get(pk=uid)
    last_submission = Measurements.objects.filter(measurer=uid).order_by('-number').first()
    if last_submission is None:
        new_num = 0
    else:
        new_num = last_submission.number + 1
    ## set output filename
    datatemp_pre: str  = 'temp/data-'
    fieldtemp_pre: str = 'temp/field-'
    dataext: str   = '.txt'
    fieldext: str  = '.xlsx'
    ## write file to temporary directory
    datatemp: str  = datatemp_pre + str(uid) + '-' + str(new_num) + dataext
    fieldtemp: str = fieldtemp_pre + str(uid) + '-' + str(new_num) + fieldext
    with open(datatemp, 'wb+') as destination:
        for chunk in datafile.chunks():
            destination.write(chunk)
    with open(fieldtemp, 'wb+') as destination:
        for chunk in fieldfile.chunks():
            destination.write(chunk)
    ## validity checking starts here
    out = {'ok': True, 'msg': []}
    fstatus = 'undetermined'
    dstatus = 'undetermined'
    ## check datafile for device type
    dev = 'undetermined'
    if not check_df_magic(datatemp):
        out['ok'] = False
        out['msg'].append("datafile magic check fails ")
        return out
    utf8_enc = check_file_encoding(datatemp)
    if not utf8_enc:
        convert_to_utf8(datatemp)
    if check_df_is_licor(datatemp):
        dev = 'licor'
    elif check_df_is_egm5(datatemp):
        dev = 'egm5'
    elif check_df_is_gasmet(datatemp):
        dev = 'gasmet'
    elif check_df_is_egm4(datatemp):
        dev = 'egm4'
    out['device'] = dev
    ## check fieldform
    if not check_ff_magic(fieldtemp):
        out['ok'] = False
        out['msg'].append("fieldform magic check fails ")
        return out
    ffp_out = get_ff_pandas(fieldtemp,dev)
    if not ffp_out['ok']:
        fstatus = 'invalid'
        out['ok'] = False
        msgs = ffp_out.get('msg')
        if msgs:
            for i in range(len(msgs)):
                out['msg'].append(msgs[i])
        else:
            out['msg'].append("errors in get_ff_pandas ")
        return out
    else:
        fstatus = 'valid'
    ## todo: check datafile (different for egm5 and licor)
    if dev == 'licor':
        rdf_out = read_df_licor(datatemp,True)
    elif dev == 'egm5':
        rdf_out = read_df_egm5(datatemp,True)
    elif dev == 'gasmet':
        rdf_out = read_df_gasmet(datatemp,True)
    elif dev == 'egm4':
        rdf_out = read_df_egm4(datatemp,True)
    else:
        rdf_out = {'ok': False, 'msg': 'device not supported or not correctly inferred '}
    if not rdf_out['ok']:
        dstatus = 'invalid'
        out['ok'] = False
        msgs = rdf_out.get('msg')
        if msgs:
            for i in range(len(msgs)):
                out['msg'].append(msgs[i])
            else:
                out['msg'].append("errors in read_df_ ")
        return out
    else:
        ## test that all series have some rows on the data file
        ## NOTE: removed because empty series are now allowed
        ##fdf = ffp_out['df']
        ##ddf = rdf_out['df']
        ##len_series = len(fdf["Date (yyyy-mm-dd)"].to_list())
        ##for i in range(len_series):
        ##    start_time = fdf.iloc[i, fdf.columns.get_loc('Start time')]
        ##    end_time = fdf.iloc[i, fdf.columns.get_loc('End time')]
        ##    if dev == 'licor':
        ##        part = ddf[(ddf['TIME'] > start_time) & (ddf['TIME'] < end_time)]
        ##    else:
        ##        part = ddf[(ddf['Time'] > start_time) & (ddf['Time'] < end_time)]
        ##    part_shape = part.shape
        ##    if part_shape[0] < 5:
        ##        out['ok'] = False
        ##        out['msg'].append("Field form row with start time " + str(start_time) +
        ##                          " has < 5 data points on the data file ")
        ##        dstatus = 'invalid'
        ##        return out
        dstatus = 'valid'
    ## everything checks ok, we are good to go
    ## write file to disk
    datapre: str   = 'subdir/data-'
    fieldpre: str  = 'subdir/field-'
    datapath: str  = datapre + str(uid) + '-' + str(new_num) + dataext
    fieldpath: str = fieldpre + str(uid) + '-' + str(new_num) + fieldext
    with open(datapath, 'wb+') as destination:
        for chunk in datafile.chunks():
            destination.write(chunk)
    with open(fieldpath, 'wb+') as destination:
        for chunk in fieldfile.chunks():
            destination.write(chunk)
    if not utf8_enc:
        convert_to_utf8(datapath)
    ## create Measurements object and store
    newmeas = Measurements(measurer      = uobj,
                           number        = new_num,
                           measure_date  = measdate,
                           comment       = comment_str,
                           device        = dev,
                           datastatus    = dstatus,
                           datafilepath  = datapath,
                           dataorigname  = dataname,
                           fieldstatus   = fstatus,
                           fieldfilepath = fieldpath,
                           fieldorigname = fieldname)
    newmeas.save()
    return out

def check_file_encoding(filepath):
    try:
        with open(filepath,"r") as rd:
            line = rd.readline()
    except UnicodeDecodeError as e:
        ## todo: use chardet and return the encoding
        return False
    else:
        return True

def convert_to_utf8(filepath):
    ## todo: read any encoding
    with open(filepath,"r",encoding="iso_8859_1") as rd:
        data = rd.read()
    f = open(filepath,"w",encoding="utf-8")
    f.write(data)
    f.close()
