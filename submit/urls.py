from django.urls import path
from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token, refresh_jwt_token

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('list/', views.measurements_list, name='measurements_list'),
    path('upload/', views.upload, name='upload'),
    path('ddetails/<str:uname>/<int:num>/', views.datadetail, name='datadetail'),
    path('fdetails/<str:uname>/<int:num>/', views.formdetail, name='formdetail'),
    path('dfilesend/<str:uname>/<int:num>/', views.senddatafile, name='datafilesend'),
    path('ffilesend/<str:uname>/<int:num>/', views.sendformfile, name='formfilesend'),
    path('statuschange/', views.statuschange, name='statuschange'),
    path('dataget/', views.dataget, name='dataget'),
    path('api/token/', obtain_jwt_token),
    path('api/token/verify/', verify_jwt_token),
    path('api/token/refresh/', refresh_jwt_token),
    path('users/', views.get_users, name='users'),
    path('submissions/', views.get_submissions, name='submissions'),
    path('submissions/<int:uid>/', views.get_submissions_uid, name='submissions_uid'),
    path('submissions/<str:uname>/', views.get_submissions_uname, name='submissions_uname'),
    path('submissions_plus/', views.get_submissions_plus, name='submissions_plus'),
    path('submission/<int:mid>/', views.get_submission, name='submission_uid'),
    path('field/<int:mid>/', views.get_field_data_id, name='field_id'),
    path('field/<int:uid>/<int:num>/', views.get_field_data_uid, name='field_uid'),
    path('dataseries/<int:mid>/<int:series_num>/',views.get_data_series, name='series')
]
